import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:teamah/helpers/constants/MyColors.dart';
import 'package:teamah/helpers/customs/RichTextFiled.dart';
import 'package:teamah/teemah/view/custom_widget/custom_text.dart';
import 'package:teamah/teemah/view/screens/change_pass/change_pass.dart';



class ProfileScreen extends StatefulWidget {
  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  File _selectImage;
  GlobalKey<FormState> _form = GlobalKey<FormState>();
  final TextEditingController _nameTextEditingController = TextEditingController();
  final TextEditingController _emailTextEditingController = TextEditingController();
  final TextEditingController _phoneTextEditingController = TextEditingController();

  final picker = ImagePicker();

  Future getImage() async {
    print("teeeest");
    final _pickedFile = await picker.getImage(
        imageQuality: 75,
        maxHeight: 300,
        maxWidth: 400,
        source: ImageSource.camera);
    setState(() {
      if (_pickedFile != null) {
        _selectImage = File(_pickedFile.path);
        print(_selectImage);
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MyColors.primary,
        title:    Padding(
          padding: const EdgeInsets.symmetric(vertical: 8,),
          child: CustomText(
            text: 'الملف الشخصية',
            fontSize: 18,
          ),
        ),

        leading:
            IconButton(onPressed: (){Navigator.pop(context);}, icon:Icon(Icons.arrow_back_ios_rounded)),

        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              height: 20,
              width: 50,
              child:
              Stack(
                  alignment: Alignment.topCenter, children: [
                Padding(
                  padding: const EdgeInsets.only(top: 15),
                  child: Image.asset('assets/images/alert.png'),
                ),
                Positioned(
                  left: 20,
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.red.shade900, shape: BoxShape.circle),
                    child: Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: Text("3", style: TextStyle(color: Colors.white),)
                    ),
                  ),
                ),
              ]),
            ),
          ) ,
        ],
      ),

      body: SafeArea(
        child: Form(
          key: _form,
          child: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                   Container(
                     margin: EdgeInsets.only(top: 10),
                     alignment: Alignment.center,
                     child:_selectImage!=null?CircleAvatar(
                        backgroundImage: FileImage(_selectImage),
                       radius: 50,
                           ): Stack(
                              alignment: Alignment.topCenter,
                              children:[
                                InkWell(
                                onTap: (){
                                  futureShowChoiceDialog(context);
                                },
                                 child:Container(
                                  margin: const EdgeInsets.only(top: 20),
                                    height: 100,
                                    decoration:BoxDecoration(
                                      shape: BoxShape.circle,
                                            color:MyColors.accent,
                                          ),
                                     child: Center(
                                         child: Icon(
                                      Icons.person,
                                      size: 60,
                                      color:MyColors.white,
                                ),
                              )
                          ),
                          ),
                           Container(
                              margin: EdgeInsets.only(left: 80,top: 20),
                              height: 50,
                              width: 30,
                              decoration: BoxDecoration(
                                color: MyColors.primary,
                                shape: BoxShape.circle
                              ),
                              child: Icon(Icons.edit,color: MyColors.white,
                              size: 20  ,),
                            )
                  ]),
                   ),
                   CustomText(text: ' الأسم  ',
                    fontSize: 14,
                    color: Colors.grey[600],
                  ),
                   RichTextFiled(
                    controller: _nameTextEditingController,
                    max: 1,
                    label: "محمود مرعي",
                    icon: Icon(Icons.edit),
                    type: TextInputType.name,
                    margin:  EdgeInsets.only(top: 5,bottom: 20),
                    action: TextInputAction.next,
                  ),
                   CustomText(text: ' البريد الألكتروني   ',
                    fontSize: 14,
                    color: Colors.grey[600],
                  ),
                   RichTextFiled(
                    controller: _emailTextEditingController,
                    max: 1,
                    label: "example@info.com",
                    icon: Icon(Icons.edit),
                    type: TextInputType.name,
                    margin:  EdgeInsets.only(top: 5,bottom: 20),
                    action: TextInputAction.next,
                  ),
                   CustomText(text: ' رقم الجوال   ',
                    fontSize: 14,
                     color: Colors.grey[600],
                  ),
                   RichTextFiled(
                    controller: _phoneTextEditingController,
                    max: 1,
                    label: "02 01058748561",
                    icon: Icon(Icons.edit),
                    type: TextInputType.name,
                    margin:  EdgeInsets.only(top: 5,),
                    action: TextInputAction.next,
                  ),
                   SizedBox(height: MediaQuery.of(context).size.height*.1 ,),
                   SizedBox(
                    height:55,
                    width: MediaQuery.of(context).size.width * 0.95,
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: MyColors.white,
                          side: const BorderSide(
                              width: 2.0,
                              color: Colors.deepOrange
                          ),
                        ),
                        onPressed: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context) =>ChangePassword()));
                        },
                        child:  CustomText(
                          text: 'تغيير كلمة المرور ',
                          fontSize: 18,
                          color: MyColors.primary,
                          fontweight: FontWeight.bold,
                        )
                    ),
                  ),
                   SizedBox(height: 20,),
                   SizedBox(
                    height:55,
                    width: MediaQuery.of(context).size.width * 0.95,
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: MyColors.primary,
                          side: const BorderSide(
                              width: 2.0,
                              color: Colors.deepOrange
                          ),
                        ),
                        onPressed: (){},
                        child:  CustomText(
                          text: 'حفظ التعديلات ',
                          fontSize: 18,
                          color: MyColors.white,
                          fontweight: FontWeight.bold,
                        )
                    ),
                  ),
                   SizedBox(height: 5,),


                ],
              ),
            ),
          ),
        ),
      ),

    );
  }

  futureShowChoiceDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Choose option", style: TextStyle(color: Colors.blue),),
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  Divider(
                    height: 1,
                    color: Colors.blue,
                  ),
                  ListTile(
                    onTap: () {
                      _openGallery(context);
                      Navigator.pop(context);
                    },
                    title: Text("Gallery"),
                    leading: Icon(
                      Icons.account_box,
                      color: Colors.blue,
                    ),
                  ),
                  Divider(
                    height: 1,
                    color: Colors.blue,
                  ),
                  ListTile(
                    onTap: () {
                      _openCamera(context);
                      Navigator.pop(context);
                    },
                    title: Text("Camera"),
                    leading: Icon(
                      Icons.camera,
                      color: Colors.blue,
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  Future<void> _openCamera(BuildContext context) async {
    print("camera");
    final _pickedFile = await picker.getImage(
        imageQuality: 50,
        maxHeight: 300,
        maxWidth: 400,
        source: ImageSource.camera);

    setState(() {
      if (_pickedFile != null) {
        _selectImage = File(_pickedFile.path);
        print(_selectImage);
      } else {
        print('No image selected.');
      }
    });
  }

  Future<void> _openGallery(BuildContext context) async {
    print("gallery");
    final _pickedFile = await picker.getImage(
        imageQuality: 50,
        maxHeight: 300,
        maxWidth: 400,
        source: ImageSource.gallery);

    setState(() {
      if (_pickedFile != null) {
        _selectImage = File(_pickedFile.path);
        print(_selectImage);
      } else {
        print('No image selected.');
      }
    });
  }
}
