import 'package:flutter/material.dart';
import 'package:teamah/helpers/constants/MyColors.dart';
import 'package:teamah/teemah/view/custom_widget/custom_text.dart';
import 'package:teamah/teemah/view/custom_widget/new_orders.dart';


class OrdersScreen extends StatelessWidget {

  TabBar get _tabBar => TabBar(
    unselectedLabelColor: MyColors.black,
    indicatorColor:Colors.deepOrange,
    labelColor:MyColors.primary ,
    tabs: const [
      Tab(child: CustomText(text: 'طلبات جديدة',fontSize: 13,),),
      Tab(child: CustomText(text: 'طلبات حالية',fontSize: 13,),),
      Tab(child: CustomText(text: 'طلبات منتهية',fontSize: 13,),),
    ],
  );

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
          backgroundColor: MyColors.secondary,
          appBar: AppBar(
            backgroundColor: MyColors.primary,
            leading:Row(
              children: [
                IconButton(onPressed: (){Navigator.pop(context);}, icon:Icon(Icons.arrow_back_ios_rounded)),
                Padding(
                 padding: const EdgeInsets.symmetric(vertical: 8,horizontal: 5),
                 child: CustomText(
                  text: 'الطلبات',
                  fontSize: 18,
               ),
             ),
              ],
            ) ,
              leadingWidth: 120,

            actions: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: SizedBox(
                  height: 20,
                  width: 50,
                  child:
                  Stack(
                      alignment: Alignment.topCenter, children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 15),
                      child: Image.asset('assets/images/alert.png'),
                    ),
                    Positioned(
                      left: 20,
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.red.shade900, shape: BoxShape.circle),
                        child: Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Text("3", style: TextStyle(color: Colors.white),)
                        ),
                      ),
                    ),
                  ]),
                ),
              ) ,


            ],
            bottom: PreferredSize(
                  preferredSize: _tabBar.preferredSize,
                  child: ColoredBox(
                  color: MyColors.secondary,
                  child: _tabBar,
                  ),
            )),
          body: TabBarView(
          children: [
            NewOrders(),
            Container(child: Center(child: CustomText(text: 'طلبات حالية',),),),
            Container(child: Center(child: CustomText(text: 'طلبات منتهية',),),),
                ],
    )));
  }
}
