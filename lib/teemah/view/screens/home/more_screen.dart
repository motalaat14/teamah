import 'package:flutter/material.dart';
import 'package:teamah/helpers/constants/MyColors.dart';
import 'package:teamah/teemah/view/custom_widget/custom_text.dart';
import 'package:teamah/teemah/view/screens/about_app/about_app_screen.dart';
import 'package:teamah/teemah/view/screens/complaints_suggestions/complaints_suggestions_screen.dart';
import 'package:teamah/teemah/view/screens/contact_us/contact_us.dart';
import 'package:teamah/teemah/view/screens/lang/lang_screen.dart';
import 'package:teamah/teemah/view/screens/money/money_screen.dart';
import 'package:teamah/teemah/view/screens/profile/profile_screen.dart';
import 'package:teamah/teemah/view/screens/questions/questions_screen.dart';
import 'package:teamah/teemah/view/screens/terms_conditions/terms_conditions_screen.dart';


class MoreScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.offWhite,
       appBar:  PreferredSize(
         preferredSize: const Size.fromHeight(70.0),
          child: AppBar(
            backgroundColor: MyColors.primary,
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
             children: [
             CustomText(
              text: "اسم الصالون",
               fontSize: 16,
              color: MyColors.white,
            ),
             CustomText(
              text: "966 55684 23",
              fontSize: 16,
              color: MyColors.white,
              ),
            ],
          ),
           leading:
           Padding(
             padding: const EdgeInsets.all(8.0),
             child: Image.asset("assets/images/usa.png"),
           ),
            actions: [
              IconButton(
                  onPressed: (){},
                  icon: const Icon(Icons.logout)
              )

            ],
         ),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              const SizedBox(height: 15,),
              ListTile(
                tileColor: MyColors.white,
                leading: const Icon(Icons.perm_identity_outlined),
                title: const CustomText(
                  text: "الملف الشخصي",
                ),
                trailing: IconButton(
                    onPressed: (){

                    },
                    icon: const Icon(Icons.arrow_forward_ios),
                ),
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => ProfileScreen(),));
                },
              ),
              const SizedBox(height: 15,),
              ListTile(
                  tileColor: MyColors.white,
                  leading: const Icon(Icons.money),
                  title: const CustomText(
                    text: "الحسابات",
                  ),
                  trailing: IconButton(
                    onPressed: (){

                    },
                    icon: const Icon(Icons.arrow_forward_ios),
                  ),
                  onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => MoneyScreen(),));
                },
              ),
              const SizedBox(height: 15,),
              ListTile(
                  tileColor: MyColors.white,
                  leading: const Icon(Icons.language),
                  title: const CustomText(
                    text: "اللغة",
                  ),
                  trailing: IconButton(
                    onPressed: (){},
                    icon: const Icon(Icons.arrow_forward_ios),
                  ),
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder:(context) =>  LanguageScreen()));
                },
              ),
              const SizedBox(height: 15,),
              ListTile(
                  tileColor: MyColors.white,
                  leading: const Icon(Icons.document_scanner),
                  title: const CustomText(
                    text: "الشكاوي والاقتراحات",
                  ),
                  trailing: IconButton(
                    onPressed: (){},
                    icon: const Icon(Icons.arrow_forward_ios),
                  ),
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder:(context) =>  ContactUs()));
                },
              ),
              const SizedBox(height: 15,),
              ListTile(
                  tileColor: MyColors.white,
                  leading: const Icon(Icons.contact_support_outlined),
                  title: const CustomText(
                    text: "الأسئلة المكررة",
                  ),
                  trailing: IconButton(
                    onPressed: (){

                    },
                    icon: const Icon(Icons.arrow_forward_ios),
                  ),
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder:(context) =>  QuestionScreen()));
                },
              ),
              const SizedBox(height: 15,),
              ListTile(
                  tileColor: MyColors.white,
                  leading: const Icon(Icons.info_outline_rounded),
                  title: const CustomText(
                    text: "عن التطبيق",
                  ),
                  trailing: IconButton(
                    onPressed: (){},
                    icon: const Icon(Icons.arrow_forward_ios),
                  ),
                onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder:(context) =>  AboutAppScreen()));
                },
              ),
              const SizedBox(height: 15,),
              ListTile(
                  tileColor: MyColors.white,
                  leading: const Icon(Icons.assignment),
                  title: const CustomText(
                    text:  "الشروط و الأحكام",
                  ),
                  trailing: IconButton(
                    onPressed: (){},
                    icon: const Icon(Icons.arrow_forward_ios),
                  ),
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder:(context) =>  TermsConditionScreen()));
                },
              ),
              SizedBox(height: 20,),

            ],
          ),
        ),
      ),
   );
  }
}
