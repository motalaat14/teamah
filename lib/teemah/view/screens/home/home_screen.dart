import 'package:flutter/material.dart';
import 'package:teamah/helpers/constants/MyColors.dart';
import 'package:teamah/teemah/view/custom_widget/custom_text.dart';
import 'package:teamah/teemah/view/screens/notification/notification_screen.dart';

import 'orders.dart';



class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _mediaQuery=MediaQuery.of(context).size;
    List<Color> colorlist=[MyColors.primary,MyColors.black,MyColors.black];
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MyColors.primary,
        title: Container(
          height: 120,
            width: 100,
            child: Image.asset('assets/images/logoWhite.png')),

        actions: [GestureDetector(
          onTap: (){
            Navigator.push(context, MaterialPageRoute(builder:(context) =>  NotificationScreen()));
          },
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: SizedBox(
              height: 20,
              width: 50,
              child:
              Stack(
                  alignment: Alignment.topCenter, children: [
                Padding(
                  padding: const EdgeInsets.only(top: 15),
                  child: Image.asset('assets/images/alert.png'),
                ),
                Positioned(
                  left: 20,
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.red.shade900, shape: BoxShape.circle),
                    child: const Padding(
                        padding: EdgeInsets.all(4.0),
                        child: Text("3", style: TextStyle(color: Colors.white),)
                    ),
                  ),
                ),
              ]),
            ),
          ),
        )],
        centerTitle: true,
        toolbarHeight: 134,
        leading: SizedBox(),
      ),
      backgroundColor: MyColors.secondary,
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                   const SizedBox(height: 20,),
                   CustomText(
                    text: '    أخر الاشعارات',
                    fontSize: 16,
                    fontweight: FontWeight.bold,
                     color: MyColors.primary,
                  ),
                   ListView.builder(
                     physics: ScrollPhysics(),
                     shrinkWrap: true,
                     itemCount:3,
                     itemBuilder: (context, index) =>
                         Container(
                           margin: EdgeInsets.symmetric(vertical: 5),
                           padding: EdgeInsets.all(5),
                           color: MyColors.accent,
                           width: _mediaQuery.width,
                           height: 39,
                           child: Row(
                             mainAxisAlignment: MainAxisAlignment.spaceBetween,
                             children: [
                               CustomText(
                                 text: 'هذا النص هو مثال لنص يمكن تغييره  ',
                                 fontSize: 14,
                                 fontweight: FontWeight.w600,
                                 color: MyColors.primary,
                               ),
                               IconButton(onPressed: (){}, icon: Icon(Icons.close,color: colorlist[index],)),


                             ],
                           ),
                         )
                     ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 10),
                      child: ListView.builder(
                        physics: ScrollPhysics(),
                        shrinkWrap: true,
                        itemCount:3,
                        itemBuilder: (context, index) =>
                            GestureDetector(
                              onTap: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context) => OrdersScreen(),));
                              },
                              child:
                              Container(
                                margin: EdgeInsets.symmetric(vertical: 10),

                                width: _mediaQuery.width,
                                height: 75,
                                decoration: BoxDecoration(
                                    color: MyColors.primary,
                                  boxShadow: [BoxShadow(
                                      color: Colors.grey[400],spreadRadius: 5,
                                      blurRadius: 7,
                                      offset: Offset(0, 3))
                                ]),
                                child: Row(
                                   mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    IconButton(onPressed: (){}, icon: Icon(Icons.note_add)),
                                    Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        CustomText(
                                          text: '100',
                                          fontSize: 14,
                                          color: MyColors.white,

                                          fontweight: FontWeight.w600,
                                        ),
                                        CustomText(
                                          text: 'الطلبات الجديدة  ',
                                          fontSize: 14,
                                          fontweight: FontWeight.w600,
                                          color: MyColors.white,
                                        ),
                                      ],
                                    ),



                                  ],
                                ),
                              ),
                            )
                  ),
                    ),
            ]),
          ),
        ));
  }
}
