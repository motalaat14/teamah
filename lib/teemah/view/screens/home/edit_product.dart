import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:teamah/helpers/constants/MyColors.dart';
import 'package:teamah/teemah/view/custom_widget/custom_text.dart';



class EditProduct extends StatefulWidget {
  @override
  _EditProductState createState() => _EditProductState();
}

class _EditProductState extends State<EditProduct> {
  GlobalKey<FormState> _form = GlobalKey<FormState>();
  final TextEditingController _productNameTextEditingController = TextEditingController();
  final TextEditingController _priceTextEditingController = TextEditingController();
  final TextEditingController _noteTextEditingController = TextEditingController();

  TextEditingController _promoCodeTextEditingController ;
  bool isActiveButton = false ;
  bool check =false;





  String dropdownvalue2 = 'حدد القسم';
  var items2 =  ['حدد القسم','Center','Clinic','Hospital','Clinics'];


  File _selectImage;
  final picker = ImagePicker();
  Future getImage() async {
    print("teeeest");
    final _pickedFile = await picker.getImage(
        imageQuality: 75,
        maxHeight: 300,
        maxWidth: 400,
        source: ImageSource.camera);
    setState(() {
      if (_pickedFile != null) {
        _selectImage = File(_pickedFile.path);
        print(_selectImage);
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final _mediaQuery=MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: MyColors.secondary,
        appBar: AppBar(
          backgroundColor: MyColors.primary,
          title:  Padding(
            padding: const EdgeInsets.symmetric(vertical: 8,),
            child: CustomText(
              text: 'تعديل المنتج',
              fontSize: 18,
            ),
          ),
          leading:
              IconButton(onPressed: (){Navigator.pop(context);}, icon:Icon(Icons.arrow_back_ios_rounded)),
          actions: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: 20,
                width: 50,
                child:
                Stack(
                    alignment: Alignment.topCenter, children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: Image.asset('assets/images/alert.png'),
                  ),
                  Positioned(
                    left: 20,
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.red.shade900, shape: BoxShape.circle),
                      child: Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Text("3", style: TextStyle(color: Colors.white),)
                      ),
                    ),
                  ),
                ]),
              ),
            ) ,
          ],
        ),
        body: SafeArea(
          child: Form(
            key: _form,
            child: SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.symmetric(vertical: 10,horizontal: 15),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(height: 10,),
                        CustomText(text: 'اسم المنتج',
                          fontSize: 14,
                        ),
                        Container(
                          height: 60,
                          child: TextFormField(
                            controller: _productNameTextEditingController,
                            obscureText: false,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              hintText: 'ادخل اسم المنتج',
                              labelStyle: TextStyle(color: Colors.grey),
                              hintStyle: TextStyle(color: Colors.grey),
                              filled: true,
                              fillColor: Colors.white,
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(12),
                                borderSide: BorderSide(
                                  color: MyColors.primary,
                                ),),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(12),
                                borderSide: BorderSide(
                                  color:  MyColors.secondary,
                                ),
                              ),
                            ),
                            validator: (value) {
                              if (value.isEmpty) {
                                return "please enter your name";
                              } else if (value.length < 3) {
                                return "name can\'t 3 letters";
                              } else {
                                return null;
                              }
                            },
                          ),
                        ),
                        CustomText(text: 'إضافة صور',
                          fontSize: 14,
                        ),
                        Container(
                            height: 60,
                            decoration: BoxDecoration(
                                color:Colors.white,
                                borderRadius: BorderRadius.circular(15)
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                _selectImage !=null? Container(
                                  height: 55,
                                  width: 60,
                                  child: Image.file(_selectImage,fit: BoxFit.fill,),):SizedBox(),
                                SizedBox(width: _mediaQuery.width*.3,),
                                IconButton(
                                  icon: Icon(Icons.camera_enhance,color: MyColors.primary,),
                                  onPressed: (){
                                    futureShowChoiceDialog(context);
                                  },
                                ),

                              ],
                            )
                        ),
                        CustomText(text: 'القسم',
                          fontSize: 14,
                        ),
                        Container(
                          height:60,
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          width: _mediaQuery.width,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),
                            border: Border.all(color: Colors.white),
                            color: Colors.white,
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Container(
                                height: 60,
                                width: _mediaQuery.width *0.8,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                      child: Row(
                                        children: [
                                          Container(
                                            height: 30,
                                            width: _mediaQuery.width *0.78,
                                            child: DropdownButton(
                                              value: dropdownvalue2,
                                              underline: SizedBox(),
                                              isExpanded:true,
                                              icon: Icon(Icons.keyboard_arrow_down,color:Colors.grey),
                                              items:items2.map((String items2) {
                                                return DropdownMenuItem(
                                                  value: items2,
                                                  child: CustomText(text: items2,fontSize: 14,),

                                                );
                                              }
                                              ).toList(),
                                              onChanged: (String newValue){
                                                setState(() {
                                                  dropdownvalue2 = newValue;
                                                });

                                              },
                                            ),
                                          ),
                                        ],),
                                    ),
                                  ],
                                ),
                              ),

                            ],
                          ),

                        ),
                        CustomText(text: 'السعر بعد الخصم',
                          fontSize: 14,
                        ),
                        Container(
                          height: 60,
                          child: TextFormField(
                            controller: _priceTextEditingController,
                            obscureText: false,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              hintText: 'حدد السعر بالريال السعودي',
                              labelStyle: TextStyle(color: Colors.grey),
                              hintStyle: TextStyle(color: Colors.grey),
                              filled: true,
                              fillColor: Colors.white,
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(12),
                                borderSide: BorderSide(
                                  color: MyColors.primary,
                                ),),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(12),
                                borderSide: BorderSide(
                                  color:  MyColors.secondary,
                                ),
                              ),
                            ),
                            validator: (value) {
                              if (value.isEmpty) {
                                return "please enter your name";
                              } else if (value.length < 3) {
                                return "name can\'t 3 letters";
                              } else {
                                return null;
                              }
                            },
                          ),
                        ),
                        CustomText(text: 'الوصف',
                          fontSize: 14,
                        ),
                        Container(
                          height: 90,
                          child: TextFormField(
                            maxLines: 2,
                            controller: _noteTextEditingController,
                            obscureText: false,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              hintText: 'ادخل الوصف هنا',
                              labelStyle: TextStyle(color: Colors.grey),
                              hintStyle: TextStyle(color: Colors.grey),
                              filled: true,
                              fillColor: Colors.white,
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(12),
                                borderSide: BorderSide(
                                  color: MyColors.primary,
                                ),),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(12),
                                borderSide: BorderSide(
                                  color:  MyColors.secondary,
                                ),
                              ),
                            ),
                            validator: (value) {
                              if (value.isEmpty) {
                                return "please enter your name";
                              } else if (value.length < 3) {
                                return "name can\'t 3 letters";
                              } else {
                                return null;
                              }
                            },
                          ),
                        ),
                        SizedBox(
                          height:55,
                          width: MediaQuery.of(context).size.width * 0.95,
                          child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                primary: MyColors.primary,
                                side: const BorderSide(
                                    width: 2.0,
                                    color: Colors.deepOrange
                                ),
                              ),
                              onPressed: (){},
                              child:  CustomText(
                                text: 'إضافة ',
                                fontSize: 18,
                                color: MyColors.white,
                                fontweight: FontWeight.bold,
                              )
                          ),
                        ),
                      ],
                    )

                  ],
                ),
              ),


            ),

          ),

        ));
  }
  futureShowChoiceDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Choose option", style: TextStyle(color: Colors.blue),),
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  Divider(
                    height: 1,
                    color: Colors.blue,
                  ),
                  ListTile(
                    onTap: () {
                      _openGallery(context);
                      Navigator.pop(context);
                    },
                    title: Text("Gallery"),
                    leading: Icon(
                      Icons.account_box,
                      color: Colors.blue,
                    ),
                  ),
                  Divider(
                    height: 1,
                    color: Colors.blue,
                  ),
                  ListTile(
                    onTap: () {
                      _openCamera(context);
                      Navigator.pop(context);
                    },
                    title: Text("Camera"),
                    leading: Icon(
                      Icons.camera,
                      color: Colors.blue,
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  Future<void> _openCamera(BuildContext context) async {
    print("camera");
    final _pickedFile = await picker.getImage(
        imageQuality: 50,
        maxHeight: 300,
        maxWidth: 400,
        source: ImageSource.camera);

    setState(() {
      if (_pickedFile != null) {
        _selectImage = File(_pickedFile.path);
        print(_selectImage);
      } else {
        print('No image selected.');
      }
    });
  }

  Future<void> _openGallery(BuildContext context) async {
    print("gallery");
    final _pickedFile = await picker.getImage(
        imageQuality: 50,
        maxHeight: 300,
        maxWidth: 400,
        source: ImageSource.gallery);

    setState(() {
      if (_pickedFile != null) {
        _selectImage = File(_pickedFile.path);
        print(_selectImage);
      } else {
        print('No image selected.');
      }
    });
  }
}
