import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts_arabic/fonts.dart';
import 'package:teamah/helpers/constants/MyColors.dart';
import 'package:teamah/helpers/customs/RichTextFiled.dart';
import 'package:teamah/teemah/view/custom_widget/custom_bootom_navigation.dart';
import 'package:teamah/teemah/view/screens/forget_pass/forget_pass.dart';
import 'package:teamah/teemah/view/screens/sign_up/sign_up_screen.dart';




class SignInScreen extends StatefulWidget{
  @override
  State<SignInScreen> createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {

  final GlobalKey<ScaffoldState> _scaffold = GlobalKey();
  final GlobalKey<FormState> _formKey = GlobalKey();

  TextEditingController mail= TextEditingController();
  TextEditingController pass= TextEditingController();




  @override
  Widget build(BuildContext context) {
    final _media =MediaQuery.of(context).size;
    return  Scaffold(
        backgroundColor: MyColors.secondary,
        key: _scaffold,
        body: SingleChildScrollView(
          child: Column(
            children: [
              Stack(
                children: [
                  Container(
                    width: _media.width,
                    height:155,
                    decoration: const BoxDecoration(
                      image: DecorationImage(
                        image: ExactAssetImage("assets/images/bground.png"),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 12.0),
                    child: IconButton(
                        onPressed: (){},
                        icon: const Icon(Icons.arrow_back_ios_rounded,color: Colors.white,)
                    ),
                  ),
                  Center(
                    child: Container(
                      margin: const EdgeInsets.only(top: 27),
                      height: 90 ,
                      width: 100,
                      decoration: const BoxDecoration(
                          image: DecorationImage(
                            image: ExactAssetImage("assets/images/logoWhite.png"),
                          )
                      ),

                    ),
                  ),
                ],
              ),
              Center(
                child: Text(
                  " أهلاً بك وبعودتك",
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.w600,
                    color: MyColors.primary,
                    fontFamily: ArabicFonts.Cairo,
                    package: 'google_fonts_arabic',
                  ),
                ),
              ),
              Center(
                child: Text("باشر بالدخول لحسابك لدينا",
                  style: TextStyle(
                    fontSize: 18,
                    color:MyColors.grey,
                    fontFamily: ArabicFonts.Cairo,
                    package: 'google_fonts_arabic',
                  ),
                ),
              ),
              const SizedBox(height: 20,),
              Container(
                child: Form(
                  key: _formKey,
                  child: Column(
                    children:  [
                      const SizedBox(height: 20),
                      RichTextFiled(
                        controller: mail,
                        label: "Email",
                        type: TextInputType.emailAddress,
                        margin: EdgeInsets.only(top: 15,),
                      ),
                      RichTextFiled(
                        controller: pass,
                        label: "password",
                        type: TextInputType.text,
                        margin: const EdgeInsets.only(top: 15,),
                        icon: Icon(Icons.visibility_rounded,color: MyColors.grey.withOpacity(.6),),
                      ),
                      SizedBox(height: 15,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          InkWell(
                            onTap: (){
                              Navigator.push(context, CupertinoPageRoute(builder: (context)=>ForgetPassword()));
                            },
                            child: Text(tr("Forget Password"),
                              style: TextStyle(
                                  fontSize: 13,
                                  color: MyColors.primary,
                                  decoration: TextDecoration.underline,
                                  fontWeight: FontWeight.bold),),
                          )
                        ],
                      ),

                      const SizedBox(height: 20,),
                      SizedBox(
                        height:55,
                        width: _media.width ,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: MyColors.primary,
                          ),
                          onPressed: (){
                            Navigator.push(context, MaterialPageRoute(builder:(context) =>  CustomBottomNavigationBar()));
                          },
                          child: const Text(
                            "إستمرار",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontFamily: ArabicFonts.Cairo,
                              package: 'google_fonts_arabic',
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(height: 20,),
                      Row(
                        children:  [
                          Expanded(child: Divider(color: MyColors.grey,)),
                          Padding(
                            padding: const EdgeInsets.only(left: 5,right: 5),
                            child: Text(
                              "ليس لديك حساب؟",
                              style: TextStyle(
                                fontSize: 18,
                                color:MyColors.grey,
                                fontFamily: ArabicFonts.Cairo,
                                package: 'google_fonts_arabic',
                              ),
                            ),
                          ),
                          Expanded(child: Divider(color: MyColors.grey,)),


                        ],
                      ),
                      const SizedBox(height: 20,),
                      SizedBox(
                        height:55,
                        width: _media.width ,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: MyColors.white,
                            side: const BorderSide(
                              width: 2.0,
                              color: Colors.orange,
                            ),
                          ),
                          onPressed: (){
                            Navigator.push(context, MaterialPageRoute(builder:(context) =>  SignUpScreen()));
                          },
                          child: Text(
                            "تسجيل جديد",
                            style: TextStyle(
                              color: MyColors.primary,
                              fontSize: 18,
                              fontFamily: ArabicFonts.Cairo,
                              package: 'google_fonts_arabic',
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                padding: const EdgeInsets.only(left: 12,right: 12),
              ),
              const SizedBox(height: 10,),

            ],
          ),
        ),
    );
  }
}


