import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:teamah/helpers/constants/DioBase.dart';
import 'package:teamah/helpers/constants/MyColors.dart';
import 'package:teamah/teemah/view/custom_widget/custom_text.dart';
import 'package:teamah/teemah/view/screens/home/add_product.dart';
import 'package:teamah/teemah/view/screens/home/edit_product.dart';
import 'package:teamah/teemah/view/screens/my_product/my_product_provider.dart';
import 'package:teamah/teemah/view/screens/notification/notification_screen.dart';

class ProudctScreen extends StatefulWidget {
  @override
  State<ProudctScreen> createState() => _ProudctScreenState();
}

class _ProudctScreenState extends State<ProudctScreen> {
  // bool loaderO = false;
  // var x ;
  // Future<void> future() async{
  //   setState(() {
  //     loaderO = true;
  //   });
  //   try{
  //     x = await Provider.of<MyProductProvider>(context, listen: false).getMyProduct();
  //     setState(() {
  //       loaderO = false;
  //     });
  //   }catch(e){
  //     setState(() {
  //       loaderO = false;
  //     });
  //     throw (e);
  //   }
  // }
  // @override
  // void initState() {
  //  // future();
  //   getAbout();
  //   super.initState();
  // }
  @override
  Widget build(BuildContext context) {
    final ref = Provider.of<MyProductProvider>(context);
    return Scaffold(
      appBar: AppBar(
          backgroundColor: MyColors.primary,
          leading:Padding(
            padding: const EdgeInsets.symmetric(vertical: 8,horizontal: 5),
            child: CustomText(
              text: 'منتجاتي',
              fontSize: 18,
              color: Colors.white,
            ),
          ) ,
          leadingWidth: 80,

          actions: [
            GestureDetector(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder:(context) =>  NotificationScreen()));
              },

              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  height: 20,
                  width: 50,
                  child:
                  Stack(
                      alignment: Alignment.topCenter, children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 15),
                      child: Image.asset('assets/images/alert.png'),
                    ),
                    Positioned(
                      left: 20,
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.red.shade900, shape: BoxShape.circle),
                        child: Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Text("3", style: TextStyle(color: Colors.white),)
                        ),
                      ),
                    ),
                  ]),
                ),
              ),
            ) ,
          ],
          ),
      body: SingleChildScrollView(
        child: Container(
          child:
          //loading  ? Center(child: CircularProgressIndicator()):
          Column(
            children: [
              SizedBox(height: 10,),
              ListView.builder(
                  physics: ScrollPhysics(),
                  shrinkWrap: true,
                  itemCount:3,
                  itemBuilder: (context, index) =>Container(
                    margin: EdgeInsets.symmetric(vertical: 10,horizontal: 10),
                    child: Card(
                      elevation: 10,
                      color: MyColors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),),
                      child: Container(
                        margin: EdgeInsets.symmetric(vertical: 10),
                        padding: EdgeInsets.symmetric(horizontal:20),
                        child: Row(
                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                CustomText(text: 'اسم المطبخ',fontSize: 14,color: MyColors.black,),
                                CustomText(text: "برجر",fontSize: 18,color: MyColors.primary,),
                                CustomText(text: '19 رس',fontSize: 18,color: MyColors.primary,),
                                Row(
                                  children: [
                                    IconButton(onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => EditProduct(),));},
                                      icon: Icon(Icons.edit),padding: EdgeInsets.all(0),alignment: Alignment.centerRight,color: MyColors.primary,),
                                    CustomText(text: 'تعديل',fontSize: 18,color: MyColors.primary,),

                                  ],
                                ),
                              ],
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  height: 100,
                                  width: 80,
                                  decoration: const BoxDecoration(
                                      image: DecorationImage(
                                        image: AssetImage('assets/images/burger.png'),
                                      )
                                  ),
                                ),
                                Row(
                                  children: [
                                    CustomText(text: 'حذف',fontSize: 18,color: MyColors.primary,),
                                    IconButton(onPressed: (){}, icon: Icon(Icons.delete),color: MyColors.primary),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ) ,),
                  )
              ),
              SizedBox(
                height:55,
                width: MediaQuery.of(context).size.width * 0.95,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: MyColors.white,
                    side: const BorderSide(
                      width: 2.0,
                      color: Colors.orange
                    ),
                  ),
                  onPressed: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => AddProduct()));
                  },
                  child:  CustomText(
                    text: 'إضافة خدمة',
                    fontSize: 18,
                    color: MyColors.primary,
                    fontweight: FontWeight.bold,
                  )
                ),
              ),
              SizedBox(height: 10,)
            ],

          ),
        ),
      ),
    );
  }
  // bool loading = true;
  // DioBase dioBase = DioBase();
  // String desc ;
  // Future getAbout() async {
  //   Map<String, String> headers = {
  //     'Authorization': "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvdGVlbWFoYXBwLmNvbVwvYXBpXC9sb2dpbiIsImlhdCI6MTYzODU0NDI1MiwiZXhwIjoxMDk2OTc0NDI1MiwibmJmIjoxNjM4NTQ0MjUyLCJqdGkiOiJTMzNNdUxNR2FsRHJraDhyIiwic3ViIjo0LCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.nJpy0MgxBN4RocouQoAtt97n6GN7vwoAERzqT2yZlpk",
  //   };
  //   dioBase.get("authProviderServices", headers: headers).then((response) {
  //     if (response.data["key"] == "success") {
  //       setState(() {loading=false;});
  //       desc = response.data["data"];
  //     } else {
  //       print("------------------------else");
  //       Fluttertoast.showToast(msg: response.data["msg"]);
  //     }
  //   });
  // }
}
