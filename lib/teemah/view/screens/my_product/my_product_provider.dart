import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:teamah/teemah/view/screens/my_product/MyProductModel.dart';

// var header = <String, String>{
//   'Content-Type': 'application/json',
//   'Authorization': "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvdGVlbWFoYXBwLmNvbVwvYXBpXC9sb2dpbiIsImlhdCI6MTYzODU0NDI1MiwiZXhwIjoxMDk2OTc0NDI1MiwibmJmIjoxNjM4NTQ0MjUyLCJqdGkiOiJTMzNNdUxNR2FsRHJraDhyIiwic3ViIjo0LCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.nJpy0MgxBN4RocouQoAtt97n6GN7vwoAERzqT2yZlpk",
// };
// BaseOptions options =  BaseOptions(
//   connectTimeout: 50000,
//   receiveTimeout: 30000,
//   followRedirects: false,
//   receiveDataWhenStatusError: true,
//   headers: header,
//   validateStatus: (status) {
//     return status < 500;
//   },
// );
class MyProductProvider extends ChangeNotifier{
  MyProductModel myProductModel;

  Future<MyProductModel> getMyProduct() async {

    Dio dio = Dio();
    const url = "https://teemahapp.com/api/authProviderServices";
    try {
      final response = await dio.get(
        url,
        options: Options(
          headers: {
            'Content-Type': 'application/json; charset=UTF-8',
            'Authorization': "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvdGVlbWFoYXBwLmNvbVwvYXBpXC9sb2dpbiIsImlhdCI6MTYzODU0NDI1MiwiZXhwIjoxMDk2OTc0NDI1MiwibmJmIjoxNjM4NTQ0MjUyLCJqdGkiOiJTMzNNdUxNR2FsRHJraDhyIiwic3ViIjo0LCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.nJpy0MgxBN4RocouQoAtt97n6GN7vwoAERzqT2yZlpk",
          },
        )
      );
      myProductModel = MyProductModel.fromJson(response.data);
       print(response);
    } on DioError catch (e) {
      if (e.response != null) {
        print('Dio error!');
        print('STATUS: ${e.response.statusCode}');
        print('DATA: ${e.response.data}');
        print('HEADERS: ${e.response.headers}');
      } else {
        print('Error sending request!');
        print(e.message);
      }
    }
    notifyListeners();
    return myProductModel;
  }
}

