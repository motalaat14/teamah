import 'dart:convert';

MyProductModel myProductModelFromJson(String str) => MyProductModel.fromJson(json.decode(str));

String myProductModelToJson(MyProductModel data) => json.encode(data.toJson());

class MyProductModel {
  MyProductModel({
    this.key,
    this.data,
    this.msg,
    this.code,
  });

  String key;
  List<Datum> data;
  String msg;
  int code;

  factory MyProductModel.fromJson(Map<String, dynamic> json) => MyProductModel(
    key: json["key"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    msg: json["msg"],
    code: json["code"],
  );

  Map<String, dynamic> toJson() => {
    "key": key,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
    "msg": msg,
    "code": code,
  };
}

class Datum {
  Datum({
    this.serviceInfo,
  });

  ServiceInfo serviceInfo;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    serviceInfo: ServiceInfo.fromJson(json["service_info"]),
  );

  Map<String, dynamic> toJson() => {
    "service_info": serviceInfo.toJson(),
  };
}

class ServiceInfo {
  ServiceInfo({
    this.id,
    this.serviceImage,
    this.serviceName,
    this.isFavorite,
    this.description,
    this.priceBefore,
    this.priceAfter,
    this.serviceRate,
    this.providerId,
    this.providerName,
  });

  int id;
  String serviceImage;
  String serviceName;
  String isFavorite;
  String description;
  String priceBefore;
  String priceAfter;
  int serviceRate;
  String providerId;
  String providerName;

  factory ServiceInfo.fromJson(Map<String, dynamic> json) => ServiceInfo(
    id: json["id"],
    serviceImage: json["service_image"],
    serviceName: json["service_name"],
    isFavorite: json["is_favorite"],
    description: json["description"],
    priceBefore: json["price_before"],
    priceAfter: json["price_after"],
    serviceRate: json["service_rate"],
    providerId: json["provider_id"],
    providerName: json["provider_name"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "service_image": serviceImage,
    "service_name": serviceName,
    "is_favorite": isFavorite,
    "description": description,
    "price_before": priceBefore,
    "price_after": priceAfter,
    "service_rate": serviceRate,
    "provider_id": providerId,
    "provider_name": providerName,
  };
}
