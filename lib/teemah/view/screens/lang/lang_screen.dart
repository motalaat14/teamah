import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts_arabic/fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:teamah/helpers/constants/MyColors.dart';
import 'package:teamah/layouts/auth/select_user/SelectUser.dart';
import 'package:teamah/teemah/view/custom_widget/custom_text.dart';


class LanguageScreen extends StatefulWidget{
  @override
  State<LanguageScreen> createState() => _LanguageScreenState();
}

class _LanguageScreenState extends State<LanguageScreen> {
  int select  ;


  static void  changeLanguage(String lang,BuildContext context)async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (lang == "en") {
      prefs.setString("lang", lang);
      context.locale = Locale('en', 'US');
    } else {
      context.locale=Locale('ar', 'EG');
      prefs.setString("lang", lang);
    }
  }
List<String> flagImages=["assets/images/usa.png","assets/images/saudiarabia.png"];
List<String> langName=["English","Arabic"];
  @override
  Widget build(BuildContext context) {
    final statusBar = MediaQuery.of(context).padding.top;
    final appBar = AppBar().preferredSize.height;
    return  Scaffold(
        backgroundColor: MyColors.offWhite,
        appBar: AppBar(
          backgroundColor: MyColors.primary,
          title: CustomText(
            text:  "اللغة",
            fontSize: 18,
            color: MyColors.white,
          ),
          leading:IconButton(
            icon: const Icon(Icons.arrow_back_ios),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          actions: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: SizedBox(
                height: 20,
                width: 50,
                child:
                Stack(
                    alignment: Alignment.topLeft,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: Image.asset('assets/images/alert.png'),
                      ),
                      Positioned(
                        left: 10,
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.red.shade900, shape: BoxShape.circle),
                          child: const Padding(
                              padding: EdgeInsets.all(4.0),
                              child: Text("3", style: TextStyle(color: Colors.white),)
                          ),
                        ),
                      ),
                    ]),
              ),
            ),
          ],
        ),
        body:  SingleChildScrollView(
          child: SizedBox(
            height: MediaQuery.of(context).size.height -statusBar -appBar,
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top:20.0),
                  child: ListView.builder(
                      itemCount: 2,
                      shrinkWrap: true,
                      physics: const ScrollPhysics(),
                      addRepaintBoundaries: true,
                      itemBuilder: (context,index){
                        return Column(
                          children: [
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children:[
                                InkWell(
                                  onTap: () {
                                    setState(() {
                                      select = index ;
                                      print(select);
                                      index == 1 ? changeLanguage("ar", context):changeLanguage("en", context);
                                      Navigator.push(context, MaterialPageRoute(builder: (c)=>SelectUser()));

                                    });
                                    },
                                  child: Container(
                                    height: 80,
                                    width: MediaQuery.of(context).size.width ,
                                    padding: const EdgeInsets.all(12),
                                    decoration: const BoxDecoration(
                                        color: Colors.white,
                                    ),
                                    child:Row(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          children: [
                                            Container(
                                                height:25 ,
                                                width:25 ,
                                                child: Image.asset(flagImages[index])),
                                            const SizedBox(width: 10,),
                                             CustomText(
                                              text: langName[index],
                                              fontSize: 20,
                                            ),
                                          ],
                                        ),

                                        select == index ?
                                        Container(
                                          height: 25,
                                          width: 20,
                                          decoration: BoxDecoration(
                                            color: MyColors.primary,
                                            shape: BoxShape.circle,
                                          ),
                                          child: const Center(
                                              child: Icon(Icons.check,size: 15,color: Colors.white,)
                                          ),
                                        ) : Container(
                                          height: 25,
                                          width: 20,
                                          decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            border: Border.all(color: MyColors.grey)
                                          ),
                                        ),

                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                           Container(height: 1,color: Colors.grey,)
                          ],
                        );
                      }
                  ),
                ),
                const Expanded(
                  child: SizedBox(),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    height:55,
                    width: MediaQuery.of(context).size.width ,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: MyColors.primary,
                      ),
                      onPressed: (){},
                      child: const Text(
                        "حفظ التعديلات",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontFamily: ArabicFonts.Cairo,
                          package: 'google_fonts_arabic',
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            )
          ),
        )
    );
  }
}