import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:teamah/helpers/constants/MyColors.dart';
import 'package:teamah/teemah/view/custom_widget/custom_text.dart';


class NotificationScreen extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    final _media =MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: MyColors.offWhite,
      appBar: AppBar(
        backgroundColor: MyColors.primary,
        title: CustomText(
          text: "الإشعارات",
          color: MyColors.white,
        ),
        leading:IconButton(
            onPressed: (){
              Navigator.pop(context);
            },
            icon:  Icon(
              Icons.arrow_back_ios_rounded,
              color: MyColors.white,
            )
        ),

      ),
      body: SingleChildScrollView(
        child: Container(
            width: _media.width,
            padding: const EdgeInsets.only(left:10.0,right: 10,top: 20),
            child: ListView.separated(
                  shrinkWrap: true,
                  physics: const ScrollPhysics(),
                  addRepaintBoundaries: true,
                  itemCount: 10,
                  itemBuilder: (context,index){
                return Container(
                  margin: const EdgeInsets.only(bottom: 5),
                  padding: const EdgeInsets.all(4),
                  width: _media.width,
                  height: 100,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    border: Border.all(color:Colors.cyan),
                    color: Colors.white,
                  ),
                 child: Row(
                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   crossAxisAlignment: CrossAxisAlignment.start,
                   children: [
                     Row(
                       children: [
                         Container(
                           width: _media.width*0.2,
                           padding: const EdgeInsets.all(4),
                           decoration: BoxDecoration(
                             shape: BoxShape.circle,
                             border: Border.all(color: MyColors.grey),
                           ),
                           child: Image.asset("assets/images/logo.png",fit: BoxFit.fill,),

                         ),
                         const SizedBox(width: 5,),
                         Column(
                           children: [
                             Container(
                               width: _media.width*0.5,
                               child: CustomText(
                                 text: "نص الاشعار , هذا النص هو مثال لنص",
                                 fontSize: 12,
                               ),
                             ),
                             Container(
                               width: _media.width*0.5,
                               child: CustomText(
                                 text: "نص الاشعار , هذا النص هو مثال لنص",
                                 fontSize: 12,
                               ),
                             ),
                           ],
                         ),
                       ],
                     ),
                     Container(
                       width: _media.width*0.2,
                       height: 100,
                       child: CustomText(
                         text: "15 أكتوبر 2020",
                         fontSize: 12,
                       ),
                     ),
                   ],
                 ),
                );
              },
                  separatorBuilder:(context,index) {
                     return const SizedBox(height: 15);
              } ,
            ),
          ),
        ),
    );
  }

}