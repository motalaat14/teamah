import 'package:flutter/material.dart';
import 'package:teamah/helpers/constants/MyColors.dart';
import 'package:teamah/teemah/view/custom_widget/custom_text.dart';


class QuestionScreen extends StatefulWidget{
  @override
  State<QuestionScreen> createState() => _QuestionScreenState();
}

class _QuestionScreenState extends State<QuestionScreen> {
  bool _expanded = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.offWhite,
      appBar:AppBar(
        backgroundColor: MyColors.primary,
        title: CustomText(
          text: "الأسئلة المكررة",
          fontSize: 18,
          color: MyColors.white,
        ),
        leading:IconButton(
          icon: const Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: SizedBox(
              height: 20,
              width: 50,
              child:
              Stack(
                  alignment: Alignment.topLeft,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Image.asset('assets/images/alert.png'),
                    ),
                    Positioned(
                      left: 10,
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.red.shade900, shape: BoxShape.circle),
                        child: const Padding(
                            padding: EdgeInsets.all(4.0),
                            child: Text("3", style: TextStyle(color: Colors.white),)
                        ),
                      ),
                    ),
                  ]),
            ),
          ),
        ],
      ),
        body:  SingleChildScrollView(
         child: Padding(
          padding: const EdgeInsets.all(15),
          child: SizedBox(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: ListView.separated(
                itemCount: 3,
                itemBuilder: (context,index){
                  return Container(
                      decoration:  BoxDecoration(
                          border: Border.all(color: MyColors.grey),
                          borderRadius: BorderRadius.circular(28)
                        ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(28),
                      child: ExpansionPanelList(
                        elevation: 0,
                        children: [
                          ExpansionPanel(
                            backgroundColor: MyColors.offWhite,
                            headerBuilder: (context, isExpanded) {
                              return const ListTile(
                                title: Text('س : هذا النص هو مثال لنص ؟', style: TextStyle(color: Colors.black),),
                              );
                            },
                            body:Container(
                              margin: const EdgeInsets.all(4),
                              padding: const EdgeInsets.all(12),
                              width:MediaQuery.of(context).size.width * 0.8,
                              child: Flexible(
                                child: CustomText(
                                  text: "هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق",
                                  color: MyColors.primary,
                                ),
                              ),
                            ),
                            isExpanded: _expanded,
                            canTapOnHeader: true,
                          ),
                        ],
                        expansionCallback: (index, isExpanded) {
                          _expanded = !_expanded;
                          setState(() {

                          });
                        },
                      ),
                    ),
                  );
                }, separatorBuilder:(context,index) {
              return const SizedBox(height: 15);
            },
            ),
          )
         )
        )
    );
  }
}
// Row(
// mainAxisAlignment: MainAxisAlignment.spaceBetween,
// children: [
// const CustomText(
// text: "س : هذا النص هو مثال لنص ؟",
// ),
// IconButton(
// onPressed: (){
//
// },
// icon: const Icon(Icons.keyboard_arrow_down_rounded)
// )
// ],
// ),