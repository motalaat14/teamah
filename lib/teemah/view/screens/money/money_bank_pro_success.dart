import 'package:flutter/material.dart';
import 'package:teamah/helpers/constants/MyColors.dart';
import 'package:teamah/teemah/view/custom_widget/custom_bootom_navigation.dart';
import 'package:teamah/teemah/view/custom_widget/custom_text.dart';

class MoneySucessScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(

          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          margin: EdgeInsets.all(20),

        child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Image.asset('assets/images/checkStatus.png'),
                CustomText(
                  text: 'تم إتمام العملية بنجاح',
                  fontSize: 18,
                  color: MyColors.black,
                ),
                SizedBox(height: MediaQuery.of(context).size.height*.4,),
                SizedBox(
                    height:55,
                    width: MediaQuery.of(context).size.width * 0.95,
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: MyColors.primary,
                          side: const BorderSide(
                              width: 2.0,
                              color: Colors.deepOrange
                          ),
                        ),
                        onPressed: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context) => CustomBottomNavigationBar(),));
                        },
                        child:  CustomText(
                          text: 'اذهب للرئيسية',
                          fontSize: 18,
                          color: MyColors.white,
                          fontweight: FontWeight.bold,
                        )
                    ),
                  ),
              ],
            ),
      ),
    );
  }
}
