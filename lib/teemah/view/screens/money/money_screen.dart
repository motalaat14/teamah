import 'package:flutter/material.dart';
import 'package:teamah/helpers/constants/MyColors.dart';
import 'package:teamah/teemah/view/custom_widget/custom_text.dart';
import 'package:teamah/teemah/view/screens/money/money_bank_pro.dart';
import 'package:teamah/teemah/view/screens/notification/notification_screen.dart';


class MoneyScreen extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    List <String> _list=['إجمالي الطلبات','اجمالي عمولة التطبيق','نسبة العمولة'];
    List<String> _num=['200','  100 ر.س',' % 2'];
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MyColors.primary,
        title:    Padding(
          padding: const EdgeInsets.symmetric(vertical: 8,),
          child: CustomText(
            text: 'الحسابات',
            fontSize: 18,
          ),
        ),

        leading:
        IconButton(onPressed: (){Navigator.pop(context);}, icon:Icon(Icons.arrow_back_ios_rounded)),

        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              height: 20,
              width: 50,
              child:
              Stack(
                  alignment: Alignment.topCenter, children: [
                Padding(
                  padding: const EdgeInsets.only(top: 15),
                  child: Image.asset('assets/images/alert.png'),
                ),
                Positioned(
                  left: 20,
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.red.shade900, shape: BoxShape.circle),
                    child: Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: Text("3", style: TextStyle(color: Colors.white),)
                    ),
                  ),
                ),
              ]),
            ),
          ) ,
        ],
      ),

      body: SingleChildScrollView(
        child: Container(

          child: Column(
            children: [
              Container(
                margin: EdgeInsets.all(18),
                child: ListView.separated(
                  separatorBuilder: (context, index) => SizedBox(height: 10,),
                  shrinkWrap: true,
                    physics: ScrollPhysics(),
                    scrollDirection: Axis.vertical,
                    itemCount:_list.length,
                    itemBuilder: (context, index) => Container(

                      child: Stack(
                        alignment: Alignment.center,
                          children:[
                            Image.asset('assets/images/card.png',),
                            Column(

                              children: [
                                CustomText(text: '${_list[index]}', fontSize: 18,color: MyColors.white,),
                                CustomText(text: '${_num[index]}', fontSize: 20,color: MyColors.white,fontweight: FontWeight.bold,),
                              ],
                            )
                          ]),
                    ),
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height*.1,),
              SizedBox(
                height:55,
                width: MediaQuery.of(context).size.width * 0.90,
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: MyColors.primary,
                      side: const BorderSide(
                          width: 2.0,
                          color: Colors.deepOrange
                      ),
                    ),
                    onPressed: (){
                      Navigator.push(context, MaterialPageRoute(builder:(context) =>  MoneyBankProScreen()));
                    },
                    child:  CustomText(
                      text: 'طلب تسوية ',
                      fontSize: 18,
                      color: MyColors.white,
                      fontweight: FontWeight.bold,
                    )
                ),
              ),
              SizedBox(height: 5,),
            ],
          ),
        ),
      ),
    );
  }
}
