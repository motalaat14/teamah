import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:teamah/helpers/constants/MyColors.dart';
import 'package:teamah/teemah/view/custom_widget/custom_text.dart';


import 'money_bank_pro_success.dart';


class MoneyBankProScreen extends StatefulWidget {

  @override
  _MoneyBankProScreenState createState() => _MoneyBankProScreenState();
}

class _MoneyBankProScreenState extends State<MoneyBankProScreen> {
  File _selectImage;
  GlobalKey<FormState> _form = GlobalKey<FormState>();
  final TextEditingController _nameTextEditingController = TextEditingController();
  final TextEditingController _emailTextEditingController = TextEditingController();
  final TextEditingController _phoneTextEditingController = TextEditingController();
  final picker = ImagePicker();

  Future getImage() async {
    print("teeeest");
    final _pickedFile = await picker.getImage(
        imageQuality: 75,
        maxHeight: 300,
        maxWidth: 400,
        source: ImageSource.camera);
    setState(() {
      if (_pickedFile != null) {
        _selectImage = File(_pickedFile.path);
        print(_selectImage);
      } else {
        print('No image selected.');
      }
    });
  }
  @override
  Widget build(BuildContext context) {
    final _mediaQuery=MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: MyColors.secondary,
      appBar: AppBar(
        backgroundColor: MyColors.primary,
        title:    Padding(
          padding: const EdgeInsets.symmetric(vertical: 8,),
          child: CustomText(
            text: 'طلب تسوية',
            fontSize: 18,
          ),
        ),

        leading:
        IconButton(onPressed: (){Navigator.pop(context);}, icon:Icon(Icons.arrow_back_ios_rounded)),


      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: 125,
              margin: EdgeInsets.only(top: 20),
              width: _mediaQuery.width,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  CustomText(
                    text: 'إجمالي المبلغ المطلوب',
                    fontSize: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CustomText(
                        text: '199',
                        color: MyColors.primary,
                        fontSize: 47,
                      ),
                      CustomText(
                        text: 'ريال \nسعودي',
                        color: MyColors.black,
                        fontSize: 8,
                      ),
                    ],
                  )

                ],
              ),

            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 10,horizontal: 20),
              width: _mediaQuery.width,
              height: 90,
              decoration: BoxDecoration(
                  color: MyColors.primary,
                  // boxShadow: [BoxShadow(
                  //     color: Colors.grey[400],spreadRadius: 5,
                  //     blurRadius: 7,
                  //     offset: Offset(0, 3))]
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CustomText(
                        text: 'رقم الحساب البنكي',
                        fontSize: 16,
                        color: MyColors.white,

                        fontweight: FontWeight.w600,
                      ),
                      CustomText(
                        text: '228846564878568',
                        fontSize: 20,
                        fontweight: FontWeight.w600,
                        color: MyColors.white,
                      ),
                    ],
                  ),



                ],
              ),
            ),
            Form(
              key: _form,
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CustomText(text: 'اسم صاحب الحساب',
                        fontSize: 16,
                        color: Colors.black,
                      ),
                      SizedBox(height: 5,),
                      Container(
                        height: 60,
                        child: TextFormField(
                          controller: _nameTextEditingController,
                          obscureText: false,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            hintText: ' برجاء إدخال اسم صاحب الحساب',
                            labelStyle: TextStyle(color: Colors.grey),
                            hintStyle: TextStyle(color: Colors.grey),
                            filled: true,
                            fillColor: Colors.white,
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(12),
                              borderSide: BorderSide(
                                color: MyColors.primary,
                              ),),
                            enabledBorder: OutlineInputBorder(

                              borderSide: BorderSide(
                                color:  MyColors.grey,
                              ),
                            ),
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              return "please enter your name";
                            } else if (value.length < 3) {
                              return "name can\'t 3 letters";
                            } else {
                              return null;
                            }
                          },
                        ),
                      ),
                      SizedBox(height: 5,),
                      CustomText(text: 'اسم المصرف',
                        fontSize: 16,
                        color: Colors.black,
                      ),
                      SizedBox(height: 5,),
                      Container(
                        height: 60,
                        child: TextFormField(
                          controller: _nameTextEditingController,
                          obscureText: false,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            hintText: ' برجاء إدخال اسم المصرف',
                            labelStyle: TextStyle(color: Colors.grey),
                            hintStyle: TextStyle(color: Colors.grey),
                            filled: true,
                            fillColor: Colors.white,
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(12),
                              borderSide: BorderSide(
                                color: MyColors.primary,
                              ),),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color:  MyColors.grey,
                              ),
                            ),
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              return "please enter your name";
                            } else if (value.length < 3) {
                              return "name can\'t 3 letters";
                            } else {
                              return null;
                            }
                          },
                        ),
                      ),
                      SizedBox(height: 5,),
                      CustomText(text: 'اسم المصرف المحول منه',
                        fontSize: 16,
                        color: Colors.black,
                      ),
                      SizedBox(height: 5,),
                      Container(
                        height: 60,
                        child: TextFormField(
                          controller: _nameTextEditingController,
                          obscureText: false,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            hintText: 'برجاء إدخال اسم المصرف المحول منه',
                            labelStyle: TextStyle(color: Colors.grey),
                            hintStyle: TextStyle(color: Colors.grey),
                            filled: true,
                            fillColor: Colors.white,
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(12),
                              borderSide: BorderSide(
                                color: MyColors.primary,
                              ),),
                            enabledBorder: OutlineInputBorder(

                              borderSide: BorderSide(
                                color:  MyColors.grey,
                              ),
                            ),
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              return "please enter your name";
                            } else if (value.length < 3) {
                              return "name can\'t 3 letters";
                            } else {
                              return null;
                            }
                          },
                        ),
                      ),
                      SizedBox(height: 5,),
                      CustomText(text: 'صورة إيصال التحويل',
                        fontSize: 16,
                        color: Colors.black,
                      ),
                      SizedBox(height: 5,),
                      Container(
                          height: 60,
                          decoration: BoxDecoration(
                              color:Colors.white,
                              borderRadius: BorderRadius.circular(15)
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              _selectImage !=null? Container(
                                height: 55,
                                width: 60,
                                child: Image.file(_selectImage,fit: BoxFit.fill,),):SizedBox(),
                              SizedBox(width: _mediaQuery.width*.3,),
                              IconButton(
                                icon: Icon(Icons.file_upload,color: MyColors.grey,),
                                onPressed: (){
                                  futureShowChoiceDialog(context);
                                },
                              ),

                            ],
                          )
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height*.06 ,),
                      SizedBox(
                        height:55,
                        width: MediaQuery.of(context).size.width * 0.95,
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: MyColors.primary,
                              side: const BorderSide(
                                  width: 2.0,
                                  color: Colors.deepOrange
                              ),
                            ),
                            onPressed: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context) => MoneySucessScreen(),));
                            },
                            child:  CustomText(
                              text: 'إستمرار',
                              fontSize: 18,
                              color: MyColors.white,
                              fontweight: FontWeight.bold,
                            )
                        ),
                      ),
                      SizedBox(height: 5,),


                    ],
                  ),
                ),
              ),

          ],
        ),
      ),
    );
  }
  futureShowChoiceDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Choose option", style: TextStyle(color: Colors.blue),),
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  Divider(
                    height: 1,
                    color: Colors.blue,
                  ),
                  ListTile(
                    onTap: () {
                      _openGallery(context);
                      Navigator.pop(context);
                    },
                    title: Text("Gallery"),
                    leading: Icon(
                      Icons.account_box,
                      color: Colors.blue,
                    ),
                  ),
                  Divider(
                    height: 1,
                    color: Colors.blue,
                  ),
                  ListTile(
                    onTap: () {
                      _openCamera(context);
                      Navigator.pop(context);
                    },
                    title: Text("Camera"),
                    leading: Icon(
                      Icons.camera,
                      color: Colors.blue,
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  Future<void> _openCamera(BuildContext context) async {
    print("camera");
    final _pickedFile = await picker.getImage(
        imageQuality: 50,
        maxHeight: 300,
        maxWidth: 400,
        source: ImageSource.camera);

    setState(() {
      if (_pickedFile != null) {
        _selectImage = File(_pickedFile.path);
        print(_selectImage);
      } else {
        print('No image selected.');
      }
    });
  }

  Future<void> _openGallery(BuildContext context) async {
    print("gallery");
    final _pickedFile = await picker.getImage(
        imageQuality: 50,
        maxHeight: 300,
        maxWidth: 400,
        source: ImageSource.gallery);

    setState(() {
      if (_pickedFile != null) {
        _selectImage = File(_pickedFile.path);
        print(_selectImage);
      } else {
        print('No image selected.');
      }
    });
  }
}
