import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts_arabic/fonts.dart';
import 'package:teamah/helpers/constants/MyColors.dart';
import 'package:teamah/teemah/view/custom_widget/custom_text.dart';


class OrderDetails extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    final _media =MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: MyColors.offWhite,
      appBar: AppBar(
        backgroundColor: MyColors.primary,
        title: CustomText(
          text: "تفاصيل الطلب",
          color: MyColors.white,
        ),
        leading:IconButton(
            onPressed: (){
              Navigator.pop(context);
            },
            icon:  Icon(
              Icons.arrow_back_ios_rounded,
              color: MyColors.white,
            )
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          width: _media.width,
          child: Padding(
            padding: const EdgeInsets.all(15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomText(
                  text: "الطلب",
                  color: MyColors.primary,
                  fontSize: 18,
                  fontweight: FontWeight.bold,
                ),
                Container(
                  width: _media.width,
                  height: 120,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                     color: MyColors.white,
                  ),
                  padding: const EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children:[
                          const CustomText(
                            text: "اسم المنتج",
                            fontSize: 16,
                          ),
                       Row(
                        children: const [
                          CustomText(
                            text: " ر.س ",
                            fontSize: 16,
                          ),
                          CustomText(
                            text: " 19 ",
                            fontSize: 16,
                          ),
                        ],
                      ),
                      const CustomText(
                        text: "الكمية 2",
                        fontSize: 16,
                      )

                    ],
                  ),
                      Column(
                        children: [
                          SizedBox(
                              height: 100,
                              width: 100,
                              child: Image.asset("assets/images/burger.png")),
                        ],
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 20,),
                CustomText(
                  text: "تفاصيل الطلب",
                  color: MyColors.primary,
                  fontSize: 18,
                  fontweight: FontWeight.bold,
                ),
                const SizedBox(height: 5,),
                Container(
                  width: _media.width,
                  height: 320,
                  padding: const EdgeInsets.all(12),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    color: MyColors.white
                  ),
                  child: Column(
                   // crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          CustomText(
                            text: "اسم العميل",
                            color: MyColors.primary,
                            fontSize: 16,
                            fontweight: FontWeight.bold,
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          CustomText(
                            text: "1 يوليو 2021",
                            color: MyColors.primary,
                            fontSize: 16,
                            fontweight: FontWeight.bold,
                          ),
                          CustomText(
                            text: ": تاريخ الطلب",
                            color: MyColors.primary,
                            fontSize: 16,
                            fontweight: FontWeight.bold,
                          ),

                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          CustomText(
                            text: "5:00 pm",
                            color: MyColors.primary,
                            fontSize: 16,
                            fontweight: FontWeight.bold,
                          ),
                          CustomText(
                            text: " :وقت الطلب",
                            color: MyColors.primary,
                            fontSize: 16,
                            fontweight: FontWeight.bold,
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          CustomText(
                            text: "1 يوليو 2021",
                            color: MyColors.primary,
                            fontSize: 16,
                            fontweight: FontWeight.bold,
                          ),
                          CustomText(
                            text: " :تاريخ التوصيل",
                            color: MyColors.primary,
                            fontSize: 16,
                            fontweight: FontWeight.bold,
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          CustomText(
                            text: "5:00 pm",
                            color: MyColors.primary,
                            fontSize: 16,
                            fontweight: FontWeight.bold,
                          ),
                          CustomText(
                            text: " :وقت التوصيل",
                            color: MyColors.primary,
                            fontSize: 16,
                            fontweight: FontWeight.bold,
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          CustomText(
                            text: "الحالة",
                            color: MyColors.primary,
                            fontSize: 16,
                            fontweight: FontWeight.bold,
                          ),
                          CustomText(
                            text: " :نوع الطلب",
                            color: MyColors.primary,
                            fontSize: 16,
                            fontweight: FontWeight.bold,
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          CustomText(
                            text: "40 ريال",
                            color: MyColors.primary,
                            fontSize: 16,
                            fontweight: FontWeight.bold,
                          ),
                          CustomText(
                            text: "  :إجمالي التكلفة",
                            color: MyColors.primary,
                            fontSize: 16,
                            fontweight: FontWeight.bold,
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          CustomText(
                            text: "أونلاين",
                            color: MyColors.primary,
                            fontSize: 16,
                            fontweight: FontWeight.bold,
                          ),
                          CustomText(
                            text: " :وسيلة السداد",
                            color: MyColors.primary,
                            fontSize: 16,
                            fontweight: FontWeight.bold,
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          CustomText(
                            text: "جيد",
                            color: MyColors.primary,
                            fontSize: 16,
                            fontweight: FontWeight.bold,
                          ),
                          CustomText(
                            text: " :الحالة",
                            color: MyColors.primary,
                            fontSize: 16,
                            fontweight: FontWeight.bold,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 30),
                SizedBox(
                  height:55,
                  width: _media.width ,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: MyColors.primary,
                    ),
                    onPressed: (){},
                    child: const Text(
                      "قبول الطلب",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontFamily: ArabicFonts.Cairo,
                        package: 'google_fonts_arabic',
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),

        ),
      ),
    );
  }

}