import 'dart:io';
import 'package:flutter/material.dart';
import 'package:google_fonts_arabic/fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:teamah/helpers/constants/MyColors.dart';
import 'package:teamah/helpers/customs/RichTextFiled.dart';
import 'package:teamah/teemah/view/screens/active_account/active_account_screen.dart';
import 'package:teamah/teemah/view/screens/sign_in/sign_in_screen.dart';




class SignUpScreen extends StatefulWidget{
  @override
  State<SignUpScreen> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  File _image;
  final picker = ImagePicker();

  Future getImage() async {
    final pickedFile = await picker.getImage(
        imageQuality: 75,
        maxHeight: 300,
        maxWidth: 400,
        source: ImageSource.camera
    );

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
        print(_image);
      } else {
        print('No image selected.');
      }
    });
  }
  final GlobalKey<ScaffoldState> _scaffold = GlobalKey();
  final GlobalKey<FormState> _formKey = GlobalKey();

  TextEditingController name=TextEditingController();

  TextEditingController phone= TextEditingController();

  TextEditingController mail= TextEditingController();

  TextEditingController bank= TextEditingController();

  TextEditingController pass= TextEditingController();

  TextEditingController pass2= TextEditingController();

  bool terms = false;

  @override
  Widget build(BuildContext context) {
    final _media =MediaQuery.of(context).size;
    return  Scaffold(
        backgroundColor: MyColors.secondary,
        key: _scaffold,
        body: SingleChildScrollView(
          child: Column(
            children: [
              Stack(
                children: [
                  Container(
                    width: _media.width,
                    height:150,
                    decoration: const BoxDecoration(
                        image: DecorationImage(
                            image: ExactAssetImage("assets/images/bground.png"),
                            fit: BoxFit.fill,
                        ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: IconButton(
                        onPressed: (){},
                        icon: const Icon(Icons.arrow_back_ios_rounded,color: Colors.white,)
                    ),
                  ),
                  Center(
                    child: Container(
                      margin: const EdgeInsets.only(top: 20),
                              height: 90 ,
                              width: 100,
                              decoration: const BoxDecoration(
                                  image: DecorationImage(
                                    image: ExactAssetImage("assets/images/logoWhite.png"),
                                  )
                              ),

                    ),
                  ),
                ],
              ),
              Center(
                child: Text(
                  "تسجيل كطاهية",
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.w600,
                    color: MyColors.primary,
                    fontFamily: ArabicFonts.Cairo,
                    package: 'google_fonts_arabic',
                  ),
                ),
              ),
              Center(
                child: Text("سجل حسابك وانضم إلينا",
                  style: TextStyle(
                    fontSize: 18,
                    color:MyColors.grey,
                    fontFamily: ArabicFonts.Cairo,
                    package: 'google_fonts_arabic',
                  ),
                ),
              ),
              const SizedBox(height: 20,),
              Container(
                child: Form(
                  key: _formKey,
                  child: Column(
                    children:  [
                      _image !=null?
                      CircleAvatar(backgroundImage:  FileImage(_image), radius: 55.0)
                           :
                        InkWell(
                        onTap: (){
                          showChoiceDialog(context);
                        },
                        child: Container(
                            height: 120,
                            decoration:BoxDecoration(
                              shape: BoxShape.circle,
                              color:Colors.grey[200],
                            ),
                            child: Center(
                              child: Icon(
                                Icons.camera_alt_rounded,
                                size: 30,
                                color:MyColors.primary,
                              ),
                            )
                        ),
                      ),
                      const SizedBox(height: 20),
                      RichTextFiled(
                        controller: name,
                        max: 1,
                        label: "name",
                        type: TextInputType.name,
                        margin: const EdgeInsets.only(top: 10,),
                        action: TextInputAction.next,
                      ),
                      RichTextFiled(
                        controller: phone,
                        label: "phone",
                        type: TextInputType.phone,
                        margin: const EdgeInsets.only(top: 15,),
                        icon: SizedBox(
                          width: 90,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: const [
                              Image(
                                image: ExactAssetImage("assets/images/saudiarabia.png"),
                                width: 25,
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 6),
                                child: Text("996+",
                                  style: TextStyle(fontSize: 13,fontWeight: FontWeight.bold),),
                              )
                            ],
                          ),
                        ),
                      ),
                      RichTextFiled(
                        controller: mail,
                        label: "Email (optional) ",
                        type: TextInputType.emailAddress,
                        margin: EdgeInsets.only(top: 15,),
                      ),
                      RichTextFiled(
                        controller: pass,
                        label: "password",
                        type: TextInputType.text,
                        margin: const EdgeInsets.only(top: 15,),
                        icon: Icon(Icons.visibility_rounded,color: MyColors.grey.withOpacity(.6),),
                      ),
                      RichTextFiled(
                        controller: pass2,
                        label:"confirmPassword",
                        type: TextInputType.text,
                        margin: const EdgeInsets.only(top: 15,),
                        icon: Icon(Icons.visibility_rounded,color: MyColors.grey.withOpacity(.6),),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 15),
                        child: Row(
                          children: [
                            InkWell(
                                onTap: (){
                                  setState(() {
                                    terms =! terms;
                                  });
                                },
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 5),
                                  child: Icon(
                                    terms ? Icons.check_box_rounded :
                                    Icons.check_box_outline_blank,
                                    color: MyColors.primary,size: 28,
                                  ),
                                )),
                            InkWell(
                                onTap:(){
                                  //  Navigator.of(context).push(MaterialPageRoute(builder: (c)=>Terms(from: "register",)));
                                },
                                child:
                                Text("AcceptTerms",
                                  style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w600,
                                    color: MyColors.offPrimary,
                                    decoration: TextDecoration.underline,
                                  ),
                                )
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(height: 20,),
                      SizedBox(
                        height:55,
                        width: _media.width ,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: MyColors.primary,
                          ),
                          onPressed: (){
                            Navigator.push(context, MaterialPageRoute(builder:(context) =>  ActiveAccount()));
                          },
                          child: const Text(
                            "إستمرار",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontFamily: ArabicFonts.Cairo,
                              package: 'google_fonts_arabic',
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(height: 20,),
                      Row(
                        children:  [
                          Expanded(child: Divider(color: MyColors.grey,)),
                          Padding(
                            padding: const EdgeInsets.only(left: 5,right: 5),
                            child: Text(
                              "لديك حساب بالفعل ؟",
                              style: TextStyle(
                                fontSize: 18,
                                color:MyColors.grey,
                                fontFamily: ArabicFonts.Cairo,
                                package: 'google_fonts_arabic',
                              ),
                            ),
                          ),
                          Expanded(child: Divider(color: MyColors.grey,)),


                        ],
                      ),
                      const SizedBox(height: 20,),
                      SizedBox(
                        height:55,
                        width: _media.width ,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: MyColors.white,
                            side: const BorderSide(
                              width: 2.0,
                              color: Colors.orange,
                            ),
                          ),
                          onPressed: (){
                            Navigator.push(context, MaterialPageRoute(builder:(context) =>  SignInScreen()));
                          },
                          child: Text(
                            "تسجيل دخول",
                            style: TextStyle(
                              color: MyColors.primary,
                              fontSize: 18,
                              fontFamily: ArabicFonts.Cairo,
                              package: 'google_fonts_arabic',
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                padding: const EdgeInsets.only(left: 12,right: 12),
              ),
              const SizedBox(height: 10,),




            ],
          ),
        ),
     );
  }
  showChoiceDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              "Choose option",
              style: TextStyle(color: Colors.blue),
            ),
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  Divider(
                    height: 1,
                    color: Colors.blue,
                  ),
                  ListTile(
                    onTap: () {
                      _openGallery(context);
                      Navigator.pop(context);
                    },
                    title: Text("Gallery"),
                    leading: Icon(
                      Icons.account_box,
                      color: Colors.blue,
                    ),
                  ),
                  Divider(
                    height: 1,
                    color: Colors.blue,
                  ),
                  ListTile(
                    onTap: () {
                      _openCamera(context);
                      Navigator.pop(context);
                    },
                    title: Text("Camera"),
                    leading: Icon(
                      Icons.camera,
                      color: Colors.blue,
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
  Future<void> _openCamera(BuildContext context) async {
    print("camera");
    final pickedFile = await picker.getImage(
        imageQuality: 50,
        maxHeight: 300,
        maxWidth: 400,
        source: ImageSource.camera
    );

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
        print(_image);
      } else {
        print('No image selected.');
      }
    });
  }
  Future<void> _openGallery(BuildContext context) async {
    print("gallery");
    final pickedFile = await picker.getImage(
        imageQuality: 50,
        maxHeight: 300,
        maxWidth: 400,
        source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
        print(_image);
      } else {
        print('No image selected.');
      }
    });
  }
}