import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:teamah/helpers/constants/MyColors.dart';
import 'package:teamah/teemah/view/screens/home/home_screen.dart';
import 'package:teamah/teemah/view/screens/home/more_screen.dart';
import 'package:teamah/teemah/view/screens/my_product/product_screen.dart';
import 'package:teamah/teemah/view_model/bottom_navigation_provider.dart';


class CustomBottomNavigationBar extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    List<Widget> _widgetoption = <Widget>[
      HomeScreen(),
      ProudctScreen(),
      MoreScreen(),

    ];
    final _ref = Provider.of<BottomNavigationProvider>(context);

    return Scaffold(
     // appBar: MainAppBar(),
      body: _widgetoption.elementAt(_ref.selectedIndex),
      bottomNavigationBar: bottomNavigationBar(_ref, context),
    );

  }

  Widget bottomNavigationBar(ref, BuildContext context) {
    return Container(
        decoration: const BoxDecoration(
          boxShadow: [
            BoxShadow(color: Colors.black38, spreadRadius: 0, blurRadius: 6),
          ],
        ),
        child: ClipRRect(
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(30.0),
            topRight: Radius.circular(30.0),
          ),
          child: BottomNavigationBar(
            backgroundColor: MyColors.primary,
            // iconSize: 24,
            currentIndex: ref.selectedIndex,
            onTap: (index) {
              ref.onItemTapped(index);
            },
            type: BottomNavigationBarType.fixed,
            selectedFontSize: 13,
            unselectedFontSize: 12,
            selectedItemColor: Colors.white,
            unselectedItemColor: MyColors.accent,
            showUnselectedLabels: true,
            selectedLabelStyle: const TextStyle(color: Colors.white,fontSize: 12,fontWeight: FontWeight.w600),
            unselectedLabelStyle: TextStyle(color:  MyColors.accent,fontSize: 12,fontWeight: FontWeight.w600),
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                  icon: Icon(Icons.home),
                  label: 'الرئيسية'
                // title: new Text(LocaleKeys.NBB_text.tr()),
              ),
              BottomNavigationBarItem(
                  icon: Icon(Icons.wysiwyg),
                  label: 'منتجاتي'
              ),

              BottomNavigationBarItem(
                  icon: Icon(Icons.more_horiz_rounded),
                  label: 'المزيد'
                // title: Text(LocaleKeys.NBB_text3.tr())),
              ),
            ],
          ),
        ));
  }
}