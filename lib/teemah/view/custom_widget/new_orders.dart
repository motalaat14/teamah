import 'package:flutter/material.dart';
import 'package:teamah/helpers/constants/MyColors.dart';
import 'package:teamah/teemah/view/custom_widget/custom_text.dart';
import 'package:teamah/teemah/view/screens/order_details/order_details_screen.dart';


class NewOrders extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: [
            SizedBox(height: 10,),
            ListView.builder(
              physics: ScrollPhysics(),
                shrinkWrap: true,
                itemCount:4,
                itemBuilder: (context, index) =>GestureDetector(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => OrderDetails()));
                  }
                  ,
                  child: Container(
                    margin: EdgeInsets.symmetric(vertical: 10,horizontal: 10),
                    child: Card(
                      elevation: 10,
                      color: MyColors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),),
                      child: Container(
                        margin: EdgeInsets.symmetric(vertical: 10),
                        padding: EdgeInsets.symmetric(horizontal:20),
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(

                                  children: [
                                    Image.asset('assets/images/logo.png',height: 25,width: 27,),
                                    CustomText(text: 'رقم الطلب',fontSize: 10,color: MyColors.primary,),
                                    CustomText(text: '100',fontSize: 10,color: MyColors.primary)
                                  ],
                                ),
                                Container(height:50,child: VerticalDivider(color: MyColors.black,)),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        CustomText(text: 'اسم العميل',color: MyColors.black,fontSize: 13,),
                                        CustomText(text: '  :  ',color: MyColors.black,fontSize: 13,),
                                        CustomText(text: 'محمود',color: MyColors.primary,fontSize: 13,),



                                      ],
                                    ),
                                    Row(
                                      children: [
                                        CustomText(text: 'نوع الطلب',color: MyColors.black,fontSize: 13,),
                                        CustomText(text: '  :  ',color: MyColors.black,fontSize: 13,),
                                        CustomText(text: 'فوري',color: MyColors.primary,fontSize: 13,),




                                      ],
                                    ),
                                    Row(
                                      children: [
                                        CustomText(text: 'عدد المنتجات ',color: MyColors.black,fontSize: 13,),
                                        CustomText(text: '  :  ',color: MyColors.black,fontSize: 13,),
                                        CustomText(text: '4',color: MyColors.primary,fontSize: 13,),



                                      ],
                                    ),

                                  ],
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        CustomText(text: ' الـمبـلــغ  : ',color: MyColors.black,fontSize: 15,),
                                        CustomText(text: '  100   ',color: MyColors.primary,fontSize: 15),


                                      ],
                                    ),
                                    Row(
                                      children: [
                                        CustomText(text: 'الـدفـع  :  ',color: MyColors.black,fontSize: 15,),
                                        CustomText(text: '  اونلاين   ',color: MyColors.primary,fontSize: 15,),


                                      ],
                                    ),

                                  ],
                                ),









                              ],
                            ),

                          ],
                        ),
                      ) ,),
                  ),
                )
            )],
        ),
      ),
    );
  }
}
